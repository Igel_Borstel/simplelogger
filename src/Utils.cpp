/*
SimpleLogger - sl
-----------------

Copyright (c) 2015 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifdef _WIN32
	#define _CRT_SECURE_NO_WARNINGS
#endif

#include "../include/Utils.hpp"
#include <string>

#include <ctime>
#include <cstddef>

using namespace std;


namespace sl
{
	string getDate()
	{
		time_t now = time(NULL);
		tm* zeit = localtime(&now);
		char cstr[30];
		strftime(cstr, 30, "%x", zeit);
		string date = cstr;
		return replaceCharacter('/', '-', date);
	}

	string getTime()
	{
		time_t now = time(NULL);
		tm* zeit = localtime(&now);
		char cstr[30];
		strftime(cstr, 30, "%c", zeit);
		string date = cstr;
		return replaceCharacter(' ', '_', replaceCharacter(':', '_', replaceCharacter('/', '_', date)));
	}

	string getTimePrefix()
	{
		string prefix = "[" + getTime() + "]";
		return prefix;
	}

	std::string replaceCharacter(char src, char erg, std::string srcstr)
	{
		for (size_t i = 0; i < srcstr.size(); ++i)
		{
			if (srcstr.at(i) == src)
			{
				srcstr.at(i) = erg;
			}
		}
		return srcstr;
	}

}