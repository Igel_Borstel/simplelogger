/*
SimpleLogger - sl
-----------------

Copyright (c) 2015 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include "../include/Logger.hpp"
#include "../include/Utils.hpp"
#include <iostream>

namespace sl
{

	bool Logger::logToFile = false;
	Logger::LogLevel Logger::globalLogLevel = Logger::LogLevel::NORMAL;
	std::ofstream Logger::logfile;
	std::string Logger::filename = "";
	bool Logger::fileOpen = false;

	void Logger::enabelFileLogging()
	{
		if(!(filename != ""))
		{
			filename = getTime() + "_log.log";
		}
		logfile.open(filename, std::ios::out | std::ios::app);
		if(!logfile)
		{
			std::cerr << "Error during logfile creation" << std::endl;
			logToFile = false;
			fileOpen = false;
		}
		else
		{
			fileOpen = true;
			logToFile = true;
		}
	}

	void Logger::disableFileLogging()
	{
		logfile.close();
		fileOpen = false;
		logToFile = false;
	}

	void Logger::writeToLog(std::string str, Logger::LogLevel lm)
	{
		if(static_cast<int>(lm) <= static_cast<int>(globalLogLevel))
		{
			std::cout << getTimePrefix() << str << std::endl;
			if(logToFile && fileOpen)
			{
				logfile << getTimePrefix() << str << std::endl;
			}
		}
	}
}