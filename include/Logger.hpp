/*
SimpleLogger - sl
-----------------

Copyright (c) 2015 Markus Fischer

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef LOGGER_HPP
#define LOGGER_HPP
#ifdef WIN32
	#ifdef SIMPLELOGGERDLL_EXPORTS
		#define SIMPLELOGGERDLL_API __declspec(dllexport) 
	#else
		#define SIMPLELOGGERDLL_API __declspec(dllimport) 
	#endif
#else
	#define SIMPLELOGGERDLL_API
#endif


#include <fstream>
#include <string>

namespace sl
{
	class Logger
	{
	public:
		enum class LogLevel
		{
			DISABLED,
			NORMAL,
			EXTENDED,
			UNUSED1,
			UNUSED2,
			DEBUG
		};
	private:
		static bool logToFile;
		static LogLevel globalLogLevel;
		static std::ofstream logfile;
		static std::string filename;
		static bool fileOpen;
	public:
		static SIMPLELOGGERDLL_API void setLogfileName(std::string);
		static SIMPLELOGGERDLL_API bool isLogToFileEnabled();
		static SIMPLELOGGERDLL_API bool isFileOpen();
		static SIMPLELOGGERDLL_API void enabelFileLogging();
		static SIMPLELOGGERDLL_API void disableFileLogging();
		static SIMPLELOGGERDLL_API void writeToLog(std::string, LogLevel = LogLevel::NORMAL);
		static SIMPLELOGGERDLL_API LogLevel getGlobalLogLevel();
		static SIMPLELOGGERDLL_API void setGlobalLogLevel(LogLevel);
	};

	inline bool Logger::isLogToFileEnabled() { return logToFile; }
	inline bool Logger::isFileOpen() { return fileOpen; }
	inline Logger::LogLevel Logger::getGlobalLogLevel() {	return globalLogLevel;	}
	inline void Logger::setGlobalLogLevel(Logger::LogLevel lm) { globalLogLevel = lm;	}
	inline void Logger::setLogfileName(std::string name)
	{
		if(!filename.empty())
		{
			filename = name;
		}
	}
}

#endif //LOGGER_HPP